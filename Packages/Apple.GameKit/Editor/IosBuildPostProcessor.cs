using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEngine;

namespace HyperTools.GameSocial.Ios
{
    public static class IosBuildPostProcessor
    {
        [PostProcessBuild(10)]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string pathToBuiltProject)
        {
            if (buildTarget == BuildTarget.iOS) OnPostProcessIosBuild(pathToBuiltProject);
        }

        private static void OnPostProcessIosBuild(string pathToBuiltProject)
        {
            var pbxProjectPath = PBXProject.GetPBXProjectPath(pathToBuiltProject);
            var pbxProject = GetPbxProject(pbxProjectPath);
            var mainTargetGuid = pbxProject.GetUnityMainTargetGuid();
            var frameworkTargetGuid = pbxProject.GetUnityFrameworkTargetGuid();

            // disable bitcode
            Log($"GameSocial.Ios: Writing bitcode changes to PBXProject {pbxProjectPath}...");
            pbxProject.SetBuildProperty(frameworkTargetGuid, "ENABLE_BITCODE", "false");
            pbxProject.SetBuildProperty(mainTargetGuid, "ENABLE_BITCODE", "false");

            // entitlements
            var entitlements = GetOrCreateEntitlements(pbxProject, pathToBuiltProject, mainTargetGuid, out var entitlementsPath);
            entitlements.root.SetBoolean("com.apple.developer.game-center", true);
            entitlements.WriteToFile(entitlementsPath);

            pbxProject.WriteToFile(pbxProjectPath);
        }

        private static PBXProject GetPbxProject(string pbxProjectPath)
        {
            var pbxProject = new PBXProject();
            pbxProject.ReadFromFile(pbxProjectPath);
            return pbxProject;
        }

        private static PlistDocument GetOrCreateEntitlements(PBXProject pbxProject, string projectPath, string targetGuid, out string entitlementsPath)
        {
            var entitlements = new PlistDocument();

            var relativePath = pbxProject.GetEntitlementFilePathForTarget(targetGuid);
            if (string.IsNullOrEmpty(relativePath))
            {
                relativePath = "project.entitlements";
                pbxProject.SetBuildProperty(targetGuid, "CODE_SIGN_ENTITLEMENTS", relativePath);
            }

            entitlementsPath = Path.Combine(projectPath, relativePath).Replace('\\', '/');
            if (File.Exists(entitlementsPath))
            {
                Log($"GameSocial.Ios: Modifing entitlements file {entitlementsPath}.");
                entitlements.ReadFromFile(entitlementsPath);
            }
            else
            {
                Log($"GameSocial.Ios: Create entitlements file {entitlementsPath}.");
                entitlements.Create();
                pbxProject.AddFile(relativePath, relativePath);
            }

            return entitlements;
        }

        #region Log

        private static void Log(string message) => Log(LogType.Log, message);

        private static void LogError(string message) => Log(LogType.Error, message);

        private static void Log(LogType logType, string message)
        {
            var lt = Application.GetStackTraceLogType(logType);
            Application.SetStackTraceLogType(logType, StackTraceLogType.None);

            switch (logType)
            {
                case LogType.Log: Debug.Log(message); break;
                case LogType.Warning: Debug.LogWarning(message); break;
                default: Debug.LogError(message); break;
            }
            Application.SetStackTraceLogType(logType, lt);
        }

        #endregion
    }
}
